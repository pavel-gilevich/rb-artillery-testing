"use strict";

const dotenv = require("dotenv");
dotenv.config();

const execSync = require("child_process").execSync;
execSync("artillery run scenarios.yaml", { stdio: "inherit" });
